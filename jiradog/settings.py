# -*- coding: utf-8 -*-
import os
import logging
from .utils import JSONFormatter
from . import __version__

os_env = os.environ


class Config(object):
    SECRET_KEY = os_env.get('JIRADOG_SECRET', 'secret-key')  # TODO: change me
    APP_DIR = os.path.abspath(os.path.dirname(__file__))  # this directory
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    ASSETS_DEBUG = False
    DEBUG_TB_ENABLED = False  # Disable Debug Toolbar
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    CACHE_TYPE = 'simple'  # Can be 'memcached', 'redis', etc.
    LOG_LEVEL = logging.DEBUG
    VERSION = __version__


class ProdConfig(Config):
    """Production configuration."""
    ENV = 'prod'
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'postgresql://localhost/example'  # TODO: change me
    DEBUG_TB_ENABLED = False  # Disable Debug Toolbar
    LOG_LEVEL = logging.INFO
    LOG_FORMATTER = JSONFormatter()


class DevConfig(Config):
    """Development configuration."""
    ENV = 'dev'
    DEBUG = True
    DB_NAME = 'dev.db'
    # Put the db file in project root
    DB_PATH = os.path.join(Config.PROJECT_ROOT, DB_NAME)
    SQLALCHEMY_DATABASE_URI = 'sqlite:///{0}'.format(DB_PATH)
    DEBUG_TB_ENABLED = True
    DEBUG_TB_PROFILER_ENABLED = True
    ASSETS_DEBUG = True  # Don't bundle / minify static assets
    CACHE_TYPE = 'simple'  # Can be 'memcached', 'redis', etc.
    LOG_LEVEL = logging.DEBUG
    LOG_FORMATTER = logging.Formatter(
        "[%(asctime)s] %(pathname)s:%(lineno)d %(levelname)s - %(message)s")


class TestConfig(Config):
    TESTING = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    WTF_CSRF_ENABLED = False  # Allows form testing
